package sample.screen;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CalcPriceService
 */
@WebServlet("/CalcPriceService")
public class CalcPriceService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CalcPriceService() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		if (session.getAttribute("message") != null)
			session.removeAttribute("message");
		if (session.getAttribute("error_message") != null)
			session.removeAttribute("error_message");

		final String BLANK = "";
		final int TABLE_ROW_NUMS = 3;
		for (int i = 0; i < TABLE_ROW_NUMS; i++) {
			String formNameProductName = "product_name" + (i + 1);
			String formnameUnitPrice = "unit_price" + (i + 1);
			String formNameQuantity = "quantity" + (i + 1);
			String formNameToatlPrice = "total_price" + (i + 1);

			String valueProductName = request.getParameter(formNameProductName);
			session.setAttribute(formNameProductName, valueProductName);
			int valueUnitPrice;
			try {
				valueUnitPrice = Integer.parseInt(request.getParameter(formnameUnitPrice));
				session.setAttribute(formnameUnitPrice, valueUnitPrice);
			} catch (NumberFormatException e) {
				// session.setAttribute("error_message", "不正" + (i + 1) + "行目： 金額計算できません（単価に入力がないため）.");
				session.setAttribute(formNameToatlPrice, BLANK);
				continue;
			}
			try {
				int valueQuantity = Integer.parseInt(request.getParameter(formNameQuantity));
				session.setAttribute(formNameQuantity, valueQuantity);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				session.setAttribute(formNameToatlPrice, BLANK);
				continue;
			}

			if (!((String) request.getParameter(formnameUnitPrice)).equals("")
					&& !((String) request.getParameter(formnameUnitPrice)).equals("")) {
				int totalPrice = Integer.parseInt(request.getParameter(formnameUnitPrice))
						* Integer.parseInt(request.getParameter(formNameQuantity));
				session.setAttribute(formNameToatlPrice, totalPrice);
			}
		}

		response.sendRedirect((String) request.getAttribute("to_distination_url"));
	}

}
