package sample.screen;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FrontController
 */
@WebServlet("/FrontController")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FrontController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String refererFullPath = request.getHeader("referer");
		String[] refererPathArray = refererFullPath.split("[/]");
		String refererPageName = refererPathArray[refererPathArray.length - 1];
		request.setAttribute("to_distination_url", refererPageName);
		String submitButtonvalue = request.getParameter("submit_button");
		String toDistinationURL = new String();
		RequestDispatcher requestDispatcher = null;

		if (refererPageName.equals("register.jsp")) {
			if (submitButtonvalue.equals("金額計算")) {
				toDistinationURL = "CalcPriceService";
				request.setAttribute("from_this_url", request.getRequestURI());
				requestDispatcher = request.getRequestDispatcher(toDistinationURL);
			}
			if (submitButtonvalue.equals("実行")) {
				toDistinationURL = "RegisterService";
				request.setAttribute("from_this_url", request.getRequestURI());
				requestDispatcher = request.getRequestDispatcher(toDistinationURL);
			}
		}

		if (refererPageName.equals("change.jsp")) {
			if (submitButtonvalue.equals("金額計算")) {
				toDistinationURL = "CalcPriceService";
				request.setAttribute("from_this_url", request.getRequestURI());
				requestDispatcher = request.getRequestDispatcher(toDistinationURL);
			}
			if (submitButtonvalue.equals("実行")) {
				toDistinationURL = "ChangeService";
				request.setAttribute("from_this_url", request.getRequestURI());
				requestDispatcher = request.getRequestDispatcher(toDistinationURL);
			}
		}

		if (refererPageName.equals("delete.jsp")) {
			if (submitButtonvalue.equals("実行")) {
				toDistinationURL = "DeleteService";
				request.setAttribute("from_this_url", request.getRequestURI());
				requestDispatcher = request.getRequestDispatcher(toDistinationURL);
			}
		}

		requestDispatcher.forward(request, response);
	}

}
