package sample.screen.properties;

public final class Constants {
	private Constants() {
	}

	public static class FilePath {
		public static final String DATASOURCE_NAME = "java:comp/env/jdbc/SampleScreen4";
	}
}
