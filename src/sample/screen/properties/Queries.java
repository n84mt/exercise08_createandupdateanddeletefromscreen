package sample.screen.properties;

public class Queries {
	private Queries() {
	}

	public static class TableName {
		public static final String T_PRODUCT = "t_product";
	}

	public static class ColumnName {
		public static final String REGISTERED_ID = "registered_id";
		public static final String PRODUCT_NAME = "product_name";
		public static final String UNIT_PRICE = "unit_price";
		public static final String QUANTITY = "quantity";
		public static final String TOTAL_PRICE = "total_price";

	}
}
