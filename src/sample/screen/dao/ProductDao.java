package sample.screen.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sample.screen.connector.DBManager;
import sample.screen.domain.Product;
import sample.screen.exception.DataAccessErrorException;
import sample.screen.properties.Queries.ColumnName;
import sample.screen.properties.Queries.TableName;

public class ProductDao {
	private static final String SQL_INSERT = "INSERT INTO " + TableName.T_PRODUCT + " (" + ColumnName.REGISTERED_ID
			+ ", " + ColumnName.PRODUCT_NAME + ", " + ColumnName.UNIT_PRICE + ", " + ColumnName.QUANTITY + ", "
			+ ColumnName.TOTAL_PRICE + ") VALUES (?, ?, ?, ?, ?);";

	private static final String SQL_FIND_MAX_ID = "SELECT MAX(" + ColumnName.REGISTERED_ID + ") AS max_id FROM "
			+ TableName.T_PRODUCT + ";";

	private static final String SQL_COUNT_RECORDS = "SELECT COUNT(*) AS records FROM " + TableName.T_PRODUCT + ";";

	private static final String SQL_DELETE_BY_ID = "DELETE FROM " + TableName.T_PRODUCT + " WHERE "
			+ ColumnName.REGISTERED_ID + " = ?;";

	private static final String SQL_UPDATE_BY_ID = "UPDATE " + TableName.T_PRODUCT + " SET " + ColumnName.PRODUCT_NAME
			+ " = ?, " + ColumnName.UNIT_PRICE + " = ?, " + ColumnName.QUANTITY + " = ?, " + ColumnName.TOTAL_PRICE
			+ " = ? WHERE " + ColumnName.REGISTERED_ID + " = ?;";

	private static final String SQL_SELECT_BY_ID = "SELECT " + ColumnName.PRODUCT_NAME + ", " + ColumnName.UNIT_PRICE
			+ ", " + ColumnName.QUANTITY + " FROM " + TableName.T_PRODUCT + " WHERE " + ColumnName.REGISTERED_ID
			+ " = ?;";

	public int insert(Product product) throws SQLException, DataAccessErrorException {
		int countRecords = 0;
		try (DBManager db = DBManager.getDBManager();
				PreparedStatement statement = db.getPreparedStatement(SQL_INSERT);) {
			statement.setInt(1, product.registeredIdValue());
			statement.setString(2, product.productNameValue());
			statement.setInt(3, product.unitPriceValue());
			statement.setInt(4, product.quantityValue());
			statement.setInt(5, product.totalPriceValue());
			try {
				countRecords = statement.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
		}
		return countRecords;
	}

	public int deleteById(int registeredId) throws SQLException, DataAccessErrorException {
		int countRecords = 0;
		try (DBManager db = DBManager.getDBManager();
				PreparedStatement statement = db.getPreparedStatement(SQL_DELETE_BY_ID);) {
			statement.setInt(1, registeredId);
			try {
				countRecords = statement.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
		}
		return countRecords;
	}

	public int findMaxId() throws SQLException, DataAccessErrorException {
		int registeredId = 0;
		if (hasNotRecords())
			return registeredId;
		try (DBManager db = DBManager.getDBManager();
				PreparedStatement statement = db.getPreparedStatement(SQL_FIND_MAX_ID);) {

			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) {
					registeredId = resultSet.getInt("max_id");
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
		}
		return registeredId;
	}

	private boolean hasNotRecords() throws SQLException, DataAccessErrorException {
		try (DBManager db = DBManager.getDBManager();
				PreparedStatement statement = db.getPreparedStatement(SQL_COUNT_RECORDS);) {

			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					int countRecords = resultSet.getInt("records");
					if (countRecords != 0)
						return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
		}
		return true;
	}

	public int autoIncrementID() throws SQLException, DataAccessErrorException {
		return findMaxId() + 1;
	}

	public int updateById(Product product) throws SQLException, DataAccessErrorException {
		int countRecords = 0;
		try (DBManager db = DBManager.getDBManager();
				PreparedStatement statement = db.getPreparedStatement(SQL_UPDATE_BY_ID);) {
			statement.setString(1, product.productNameValue());
			statement.setInt(2, product.unitPriceValue());
			statement.setInt(3, product.quantityValue());
			statement.setInt(4, product.totalPriceValue());
			statement.setInt(5, product.registeredIdValue());
			try {
				countRecords = statement.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
		}
		return countRecords;
	}

	public Product findById(int registeredId) throws SQLException, DataAccessErrorException {
		Product productFindById = null;

		try (DBManager db = DBManager.getDBManager();
				PreparedStatement statement = db.getPreparedStatement(SQL_SELECT_BY_ID);) {
			statement.setInt(1, registeredId);

			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) {
					String productName = resultSet.getString(ColumnName.PRODUCT_NAME);
					int unitPrice = resultSet.getInt(ColumnName.UNIT_PRICE);
					int quantity = resultSet.getInt(ColumnName.QUANTITY);
					productFindById = new Product.Builder().registeredId(registeredId).productName(productName)
							.unitPrice(unitPrice).quantity(quantity).build();
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessErrorException("Occurred DataAccessErrorException: " + e.getMessage());
		}
		return productFindById;
	}
}
