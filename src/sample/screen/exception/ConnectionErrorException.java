package sample.screen.exception;

public class ConnectionErrorException extends Exception {
	public ConnectionErrorException(String string) {
		super(string);
	}
}
