package sample.screen.exception;

public class DataAccessErrorException extends Exception {
	public DataAccessErrorException(String string) {
		super(string);
	}
}
