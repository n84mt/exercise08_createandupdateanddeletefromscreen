package sample.screen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.screen.dao.ProductDao;
import sample.screen.domain.Product;
import sample.screen.exception.DataAccessErrorException;

/**
 * Servlet implementation class RegisterService
 */
@WebServlet("/RegisterService")
public class RegisterService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterService() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("message") != null)
			session.removeAttribute("message");
		if (session.getAttribute("error_message") != null)
			session.removeAttribute("error_message");

		ProductDao dao = new ProductDao();
		int nextRegisteredID = 0;
		try {
			nextRegisteredID = dao.autoIncrementID();
		} catch (SQLException e) {
			session.setAttribute("error_message", "不正： レコードを登録できませんでした.");
			e.printStackTrace();
		} catch (DataAccessErrorException e) {
			session.setAttribute("error_message", "不正： データベースへの接続で問題が起きました.");
			e.printStackTrace();
		}

		int countRecords = 0;
		final String BLANK = "";
		final int TABLE_ROW_NUMS = 3;
		for (int i = 0; i < TABLE_ROW_NUMS; i++) {
			String formNameRegisteredId = "registered_id" + (i + 1);
			String formNameProductName = "product_name" + (i + 1);
			String formnameUnitPrice = "unit_price" + (i + 1);
			String formNameQuantity = "quantity" + (i + 1);
			String formNameTotalPrice = "total_price" + (i + 1);

			if ((request.getParameter(formNameTotalPrice)).equals("")
					&& (!(request.getParameter(formnameUnitPrice)).equals("")
							&& !(request.getParameter(formNameQuantity)).equals(""))) {
				session.setAttribute(formNameProductName, request.getParameter(formNameProductName));
				session.setAttribute(formnameUnitPrice, request.getParameter(formnameUnitPrice));
				session.setAttribute(formNameQuantity, request.getParameter(formNameQuantity));
				session.setAttribute("error_message", "不正： 金額計算が実施されていません（" + (i + 1) + "行目）.");
				continue;
			}

			if (!(request.getParameter(formNameRegisteredId)).equals("")) {
				session.setAttribute("error_message", "不正： 既に登録されています（" + (i + 1) + "行目）.");
				continue;
			}

			List<String> formNames = new ArrayList<>(
					Arrays.asList(formNameProductName, formnameUnitPrice, formNameQuantity, formNameTotalPrice));
			if (isNotFullThisRow(request, formNames))
				continue;

			String valueProductName = request.getParameter(formNameProductName);
			int valueUnitPrice = Integer.parseInt(request.getParameter(formnameUnitPrice));
			int valueQuantity = Integer.parseInt(request.getParameter(formNameQuantity));
			int valueTotalPrice = Integer.parseInt(request.getParameter(formNameTotalPrice));

			session.setAttribute(formNameProductName, valueProductName);
			session.setAttribute(formnameUnitPrice, valueUnitPrice);
			session.setAttribute(formNameQuantity, valueQuantity);
			session.setAttribute(formNameTotalPrice, valueTotalPrice);

			Product product = new Product.Builder().registeredId(nextRegisteredID).productName(valueProductName)
					.unitPrice(valueUnitPrice).quantity(valueQuantity).build();

			if (isNotDoneCalcTotalPrice(product, valueTotalPrice)) {
				session.setAttribute("error_message", "不正： 入力値変更後に金額計算が実施されていません.");
				continue;
			}

			try {
				dao.insert(product);
			} catch (SQLException e) {
				session.setAttribute("error_message", "不正： レコードを登録できませんでした.");
				e.printStackTrace();
			} catch (DataAccessErrorException e) {
				session.setAttribute("error_message", "不正： データベースへの接続で問題が起きました.");
				e.printStackTrace();
			}

			session.setAttribute(formNameRegisteredId, nextRegisteredID);
			nextRegisteredID++;
			countRecords++;
		}
		if (countRecords != 0) {
			session.setAttribute("message", countRecords + "件のレコードを登録しました.");
			session.setAttribute("error_message", BLANK);
		}
		response.sendRedirect("register.jsp");
	}

	private boolean isNotFullThisRow(HttpServletRequest request, List<String> formNames) {
		for (String formName : formNames) {
			if (request.getParameter(formName) == "")
				return true;
		}
		return false;
	}

	private boolean isNotDoneCalcTotalPrice(Product product, int totalPrice) {
		return (product.unitPriceValue() * product.quantityValue()) != totalPrice;
	}
}
