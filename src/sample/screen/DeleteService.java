package sample.screen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.screen.dao.ProductDao;
import sample.screen.exception.DataAccessErrorException;

/**
 * Servlet implementation class ChangeService
 */
@WebServlet("/DeleteService")
public class DeleteService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteService() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("message") != null)
			session.removeAttribute("message");
		if (session.getAttribute("error_message") != null)
			session.removeAttribute("error_message");

		ProductDao dao = new ProductDao();

		int countRecords = 0;
		final String BLANK = "";
		final int TABLE_ROW_NUMS = 3;
		for (int i = 0; i < TABLE_ROW_NUMS; i++) {
			String formNameCheckbox = "checkbox" + (i + 1);
			String formNameRegisteredId = "registered_id" + (i + 1);
			String formNameProductName = "product_name" + (i + 1);
			String formnameUnitPrice = "unit_price" + (i + 1);
			String formNameQuantity = "quantity" + (i + 1);
			String formNameTotalPrice = "total_price" + (i + 1);

			List<String> formNames = new ArrayList<>(Arrays.asList(formNameCheckbox, formNameRegisteredId,
					formNameProductName, formnameUnitPrice, formNameQuantity, formNameTotalPrice));
			if (isNotFullThisRow(request, formNames))
				continue;

			int valueRegisteredId = Integer.parseInt(request.getParameter(formNameRegisteredId));
			String valueProductName = request.getParameter(formNameProductName);
			int valueUnitPrice = Integer.parseInt(request.getParameter(formnameUnitPrice));
			int valueQuantity = Integer.parseInt(request.getParameter(formNameQuantity));
			int valueTotalPrice = Integer.parseInt(request.getParameter(formNameTotalPrice));

			session.setAttribute(formNameRegisteredId, valueRegisteredId);
			session.setAttribute(formNameProductName, valueProductName);
			session.setAttribute(formnameUnitPrice, valueUnitPrice);
			session.setAttribute(formNameQuantity, valueQuantity);
			session.setAttribute(formNameTotalPrice, valueTotalPrice);

			try {
				dao.deleteById(valueRegisteredId);
			} catch (SQLException e) {
				session.setAttribute("error_message", "不正： レコードを登録できませんでした.");
				e.printStackTrace();
			} catch (DataAccessErrorException e) {
				session.setAttribute("error_message", "不正： データベースへの接続で問題が起きました.");
				e.printStackTrace();
			}
			session.setAttribute(formNameRegisteredId, BLANK);
			session.setAttribute(formNameProductName, BLANK);
			session.setAttribute(formnameUnitPrice, BLANK);
			session.setAttribute(formNameQuantity, BLANK);
			session.setAttribute(formNameTotalPrice, BLANK);
			countRecords++;
		}
		if (countRecords != 0)
			session.setAttribute("message", countRecords + "件のレコードを削除しました.");
		response.sendRedirect("delete.jsp");
	}

	private boolean isNotFullThisRow(HttpServletRequest request, List<String> formNames) {
		for (String formName : formNames) {
			if (request.getParameter(formName) == "" || request.getParameter(formName) == null)
				return true;
		}
		return false;
	}

}
