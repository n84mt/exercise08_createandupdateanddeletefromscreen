package sample.screen.domain;

import java.util.ArrayList;
import java.util.List;

public class Products {
	private List<Product> products;

	public Products() {
	}

	public Products(List<Product> products) {
		this.products = products;
	}

	public List<Product> deepCopyListValue() {
		List<Product> copy = new ArrayList<>(this.products);
		return copy;
	}
}
