package sample.screen.domain;

public class Product {
	private int registeredId;
	private String productName;
	private int unitPrice;
	private int quantity;
	private int totalPrice;

	public static class Builder {
		private int registeredId;
		private String productName;
		private int unitPrice;
		private int quantity;
		private int totalPrice;

		public Builder registeredId(int val) {
			this.registeredId = val;
			return this;
		}

		public Builder productName(String val) {
			this.productName = val;
			return this;
		}

		public Builder unitPrice(int val) {
			this.unitPrice = val;
			return this;
		}

		public Builder quantity(int val) {
			this.quantity = val;
			return this;
		}

		public Product build() {
			this.totalPrice = new Integer(this.unitPrice * quantity);
			return new Product(this);
		}
	}

	Product(Builder builder) {
		this.registeredId = builder.registeredId;
		this.productName = builder.productName;
		this.unitPrice = builder.unitPrice;
		this.quantity = builder.quantity;
		this.totalPrice = builder.totalPrice;
	}

	public int registeredIdValue() {
		return new Integer(this.registeredId);
	}

	public String productNameValue() {
		return new String(this.productName);
	}

	public int unitPriceValue() {
		return new Integer(this.unitPrice);
	}

	public int quantityValue() {
		return new Integer(this.quantity);
	}

	public int totalPriceValue() {
		return new Integer(this.totalPrice);
	}

}
