package sample.screen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.screen.dao.ProductDao;
import sample.screen.domain.Product;
import sample.screen.exception.DataAccessErrorException;

/**
 * Servlet implementation class ChangeService
 */
@WebServlet("/ChangeService")
public class ChangeService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangeService() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("message") != null)
			session.removeAttribute("message");
		if (session.getAttribute("error_message") != null)
			session.removeAttribute("error_message");

		ProductDao dao = new ProductDao();

		int countRecords = 0;
		final int TABLE_ROW_NUMS = 3;
		for (int i = 0; i < TABLE_ROW_NUMS; i++) {
			String formNameCheckbox = "checkbox" + (i + 1);
			String formNameRegisteredId = "registered_id" + (i + 1);
			String formNameProductName = "product_name" + (i + 1);
			String formnameUnitPrice = "unit_price" + (i + 1);
			String formNameQuantity = "quantity" + (i + 1);
			String formNameTotalPrice = "total_price" + (i + 1);

			List<String> formNames = new ArrayList<>(Arrays.asList(formNameCheckbox, formNameProductName,
					formnameUnitPrice, formNameQuantity, formNameTotalPrice));

			if (isNotFullThisRow(request, formNames))
				continue;

			String valueProductName = request.getParameter(formNameProductName);
			int valueUnitPrice = Integer.parseInt(request.getParameter(formnameUnitPrice));
			int valueQuantity = Integer.parseInt(request.getParameter(formNameQuantity));
			int valueTotalPrice = Integer.parseInt(request.getParameter(formNameTotalPrice));

			session.setAttribute(formNameProductName, valueProductName);
			session.setAttribute(formnameUnitPrice, valueUnitPrice);
			session.setAttribute(formNameQuantity, valueQuantity);
			session.setAttribute(formNameTotalPrice, valueTotalPrice);

			if (isNotAlreadyRegistered(request, formNameRegisteredId)) {
				session.setAttribute("error_message", "不正： 未登録のため修正できません（" + (i + 1) + "行目）.");
				continue;
			}

			int valueRegisteredId = Integer.parseInt(request.getParameter(formNameRegisteredId));
			session.setAttribute(formNameRegisteredId, valueRegisteredId);

			Product productFindById = null;

			Product product = new Product.Builder().registeredId(valueRegisteredId).productName(valueProductName)
					.unitPrice(valueUnitPrice).quantity(valueQuantity).build();

			if (isNotDoneCalcTotalPrice(product, valueTotalPrice)) {
				session.setAttribute("error_message", "不正： 入力値変更後に金額計算が実施されていません（" + (i + 1) + "行目）.");
				continue;
			}

			try {
				productFindById = dao.findById(valueRegisteredId);
			} catch (SQLException e) {
				session.setAttribute("error_message", "不正： レコードを登録できませんでした.");
				e.printStackTrace();
			} catch (DataAccessErrorException e) {
				session.setAttribute("error_message", "不正： データベースへの接続で問題が起きました.");
				e.printStackTrace();
			}

			if (isEqualBeforeAndAfter(productFindById, product)) {
				session.setAttribute("error_message", "不正： 内容が修正されていません（" + (i + 1) + "行目）.");
				continue;
			}

			try {
				dao.updateById(product);
			} catch (SQLException e) {
				session.setAttribute("error_message", "不正： レコードを登録できませんでした.");
				e.printStackTrace();
			} catch (DataAccessErrorException e) {
				session.setAttribute("error_message", "不正： データベースへの接続で問題が起きました.");
				e.printStackTrace();
			}
			countRecords++;
		}
		if (countRecords != 0)
			session.setAttribute("message", countRecords + "件のレコードを修正しました.");
		response.sendRedirect("change.jsp");
	}

	private boolean isNotAlreadyRegistered(HttpServletRequest request, String formNameRegisteredId) {
		return request.getParameter(formNameRegisteredId).equals("");
	}

	private boolean isEqualBeforeAndAfter(Product productFindById, Product product) {
		return productFindById.productNameValue().equals(product.productNameValue())
				&& productFindById.unitPriceValue() == product.unitPriceValue()
				&& productFindById.quantityValue() == product.quantityValue();
	}

	private boolean isNotDoneCalcTotalPrice(Product product, int totalPrice) {
		return (product.unitPriceValue() * product.quantityValue()) != totalPrice;
	}

	private boolean isNotFullThisRow(HttpServletRequest request, List<String> formNames) {
		for (String formName : formNames) {
			if (request.getParameter(formName) == "" || request.getParameter(formName) == null)
				return true;
		}
		return false;
	}

}
