package sample.screen.connector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import sample.screen.exception.ConnectionErrorException;
import sample.screen.properties.Constants;

public class DBManager implements AutoCloseable {

	private static DBManager db = null;
	private static DataSource dataSource = null;
	private Connection connection = null;
	private final String datasourceName = Constants.FilePath.DATASOURCE_NAME;

	private DBManager() throws NamingException, SQLException, ConnectionErrorException {
		InitialContext intitCtx = new InitialContext();
		DBManager.dataSource = (DataSource) intitCtx.lookup(this.datasourceName);
	}

	public synchronized static DBManager getDBManager()
			throws NamingException, SQLException, ConnectionErrorException {
		if (isNullDBManager())
			DBManager.db = new DBManager();
		return DBManager.db;
	}

	private static boolean isNullDBManager() {
		return DBManager.db == null;
	}

	public Connection getConnection() throws ConnectionErrorException {
		try (Connection connection = this.connection) {
			if (this.connection == null || this.connection.isClosed())
				this.connection = DBManager.dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ConnectionErrorException("Occurred ConnectionErrorException: " + e.getMessage());
		}
		return connection;
	}

	public void closeConnection() throws ConnectionErrorException {
		try (Connection connection = this.connection) {
			this.connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ConnectionErrorException("Occurred ConnectionErrorException: " + e.getMessage());
		}
	}

	public PreparedStatement getPreparedStatement(String sql) throws Exception {
		return getConnection().prepareStatement(sql);
	}

	public void commit() throws ConnectionErrorException {
		try (Connection connection = this.connection) {
			this.connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ConnectionErrorException("Occurred ConnectionErrorException: " + e.getMessage());
		}
	}

	public void rollback() throws ConnectionErrorException {
		try (Connection connection = this.connection) {
			this.connection.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ConnectionErrorException("Occurred ConnectionErrorException: " + e.getMessage());
		}
	}

	@Override
	public void close() throws ConnectionErrorException {
		try (Connection connection = this.connection) {
			this.connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ConnectionErrorException("ConnectionErrorException: " + e.getMessage());
		}
	}
}