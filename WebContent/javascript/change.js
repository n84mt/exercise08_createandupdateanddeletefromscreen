function isFull3Forms() {
	var TABLE_ROW_NUMS = 3;
	var countThisRowAllForm = 0;
	for (var i = 0; i < TABLE_ROW_NUMS; i++) {
		var formNameProductName = "product_name" + (i + 1);
		var formnameUnitPrice = "unit_price" + (i + 1);
		var formNameQuantity = "quantity" + (i + 1);

		var valueProductName = document.getElementsByName(formNameProductName)[0].value;
		var valueUnitPrice = document.getElementsByName(formnameUnitPrice)[0].value;
		var valueQuantity = document.getElementsByName(formNameQuantity)[0].value;

		if (valueProductName != "" && valueUnitPrice != ""
				&& valueQuantity != "") {
			countThisRowAllForm++;
		}
		if (countThisRowAllForm == 0)
			document.querySelector('.calc_price-button').disabled = true;
		if (countThisRowAllForm != 0)
			document.querySelector('.calc_price-button').disabled = false;
	}
}

function isCheckedAndFullThisForm() {
	var TABLE_ROW_NUMS = 3;
	var countThisRowAllForm = 0;
	for (var i = 0; i < TABLE_ROW_NUMS; i++) {
		var formNameCheckbox = "checkbox" + (i + 1);
		var formNameProductName = "product_name" + (i + 1);
		var formnameUnitPrice = "unit_price" + (i + 1);
		var formNameQuantity = "quantity" + (i + 1);
		var formNameToatlPrice = "total_price" + (i + 1);

		var valueCheckbox = document.getElementsByName(formNameCheckbox)[0].checked;
		var valueProductName = document.getElementsByName(formNameProductName)[0].value;
		var valueUnitPrice = document.getElementsByName(formnameUnitPrice)[0].value;
		var valueQuantity = document.getElementsByName(formNameQuantity)[0].value;
		var totalPriceValue = document.getElementsByName(formNameToatlPrice)[0].value;

		if (valueCheckbox && valueProductName != "" && valueUnitPrice != ""
				&& valueQuantity != "" && totalPriceValue != "") {
			countThisRowAllForm++;
		}
	}
	if (countThisRowAllForm == 0)
		document.querySelector('.register-button').disabled = true;
	if (countThisRowAllForm != 0)
		document.querySelector('.register-button').disabled = false;
}

window.onload = function() {
	isFull3Forms();
};
