function isFullThisRowAllForm() {
	var TABLE_ROW_NUMS = 3;
	var countThisRowAllForm = 0;
	for (var i = 0; i < TABLE_ROW_NUMS; i++) {
		var formNameProductName = "product_name" + (i + 1);
		var formnameUnitPrice = "unit_price" + (i + 1);
		var formNameQuantity = "quantity" + (i + 1);

		var valueProductName = document.getElementsByName(formNameProductName)[0].value;
		var valueUnitPrice = document.getElementsByName(formnameUnitPrice)[0].value;
		var valueQuantity = document.getElementsByName(formNameQuantity)[0].value;

		if (valueProductName != "" && valueUnitPrice != ""
				&& valueQuantity != "") {
			countThisRowAllForm++;
		}
	}
	if (countThisRowAllForm == 0)
		document.querySelector('.calc_price-button').disabled = true;
	if (countThisRowAllForm != 0)
		document.querySelector('.calc_price-button').disabled = false;
}

function hasTotalPrice() {
	var TABLE_ROW_NUMS = 3;
	for (var i = 0; i < TABLE_ROW_NUMS; i++) {
		var formNameToatlPrice = "total_price" + (i + 1);
		var totalPriceValue = document.getElementsByName(formNameToatlPrice)[0].value;
		if (isFullThisTotalPriceForm(totalPriceValue)) {
			document.querySelector('.register-button').disabled = false;
		}
	}
}

function isFullThisTotalPriceForm(totalPriceValue) {
	return totalPriceValue != "";
}

window.onload = function() {
	hasTotalPrice();
	isFullThisRowAllForm();
};
