<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML SYSTEM "about:legacy-compat">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/main.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/javascript/register.js"></script>
<title>登録画面</title>
</head>
<%
	final int TABLE_ROW_NUMS = 3;
	final String BLANK = "";
	for (int i = 0; i < TABLE_ROW_NUMS; i++) {
		String formNameRegisteredId = "registered_id" + (i + 1);
		String formNameProductName = "product_name" + (i + 1);
		String formnameUnitPrice = "unit_price" + (i + 1);
		String formNameQuantity = "quantity" + (i + 1);
		String formNameToatlPrice = "total_price" + (i + 1);

		////// セッション情報の null チェック
		// メッセージ
		Object sessionMessageValue = session.getAttribute("message");
		if (sessionMessageValue == null)
			session.setAttribute("message", BLANK);
		// エラーメッセージ
		Object sessionErrorMessageValue = session.getAttribute("error_message");
		if (sessionErrorMessageValue == null)
			session.setAttribute("error_message", BLANK);
		// 登録ID
		Object sessionRegisteredIdValue = session.getAttribute(formNameRegisteredId);
		if (sessionRegisteredIdValue == null)
			session.setAttribute(formNameRegisteredId, BLANK);
		// 品名
		Object sessionProductNameValue = session.getAttribute(formNameProductName);
		if (sessionProductNameValue == null)
			session.setAttribute(formNameProductName, BLANK);
		// 単価
		Object sessionUnitPriceValue = session.getAttribute(formnameUnitPrice);
		if (sessionUnitPriceValue == null)
			session.setAttribute(formnameUnitPrice, BLANK);
		// 数量
		Object sessionQuantityValue = session.getAttribute(formNameQuantity);
		if (sessionQuantityValue == null)
			session.setAttribute(formNameQuantity, BLANK);
		// 金額
		Object sessionTotalPriceValue = session.getAttribute(formNameToatlPrice);
		if (sessionTotalPriceValue == null)
			session.setAttribute(formNameToatlPrice, BLANK);
	}
%>
<body>
	<article>
		<form class="radio_buttons">
			<input type="radio" class="radio_button_register"
				value="FrontRegisterJsp" onclick="location.href=this.value" checked /><label
				for="radio_button_register">登録</label> <input type="radio"
				class="radio_button_change" value="FrontChangeJsp"
				onclick="location.href=this.value" /><label
				for="radio_button_change">修正</label> <input type="radio"
				class="radio_button_delete" value="FrontDeleteJsp"
				onclick="location.href=this.value" /><label
				for="radio_button_delete">削除</label>
		</form>

		<span class="message"><%=session.getAttribute("message")%></span> <span
			class="error_message"><%=session.getAttribute("error_message")%></span>

		<form class="transition" action="FrontController" method="post">
			<table id="form" class="form_table">
				<tr>
					<th>登録ID</th>
					<th>処理</th>
					<th>品名</th>
					<th>単価</th>
					<th>数量</th>
					<th>金額</th>
				</tr>
				<%
					for (int i = 0; i < TABLE_ROW_NUMS; i++) {
						String formNameRegisteredId = "registered_id" + (i + 1);
						String formNameCheckbox = "checkbox" + (i + 1);
						String formNameProductName = "product_name" + (i + 1);
						String formnameUnitPrice = "unit_price" + (i + 1);
						String formNameQuantity = "quantity" + (i + 1);
						String formNameToatlPrice = "total_price" + (i + 1);
				%>
				<tr>
					<td class="off_column id_column"><input type="text"
						name="<%=formNameRegisteredId%>"
						value="<%=session.getAttribute(formNameRegisteredId)%>"
						readonly="readonly" /></td>
					<td class="off_column"><input type="checkbox"
						name="<%=formNameCheckbox%>" checked disabled /></td>
					<td><input type="text" pattern=".{0,30}" title="30文字以内"
						onChange="isFullThisRowAllForm()" name="<%=formNameProductName%>"
						value="<%=session.getAttribute(formNameProductName)%>" /></td>
					<td><input type="text" pattern="[0-9]{0,6}" title="半角数字で6桁以内"
						onChange="isFullThisRowAllForm()" name="<%=formnameUnitPrice%>"
						value="<%=session.getAttribute(formnameUnitPrice)%>" />&nbsp;円</td>
					<td><input type="text" pattern="[0-9]{0,4}" title="半角数字で4桁以内"
						onChange="isFullThisRowAllForm()" name="<%=formNameQuantity%>"
						value="<%=session.getAttribute(formNameQuantity)%>" />&nbsp;個</td>
					<td class="off_column"><input class="total_price-input_text"
						type="text" pattern="[0-9]{0,4}" title="半角数字で4桁以内"
						name="<%=formNameToatlPrice%>"
						value="<%=session.getAttribute(formNameToatlPrice)%>"
						readonly="readonly" />&nbsp;円</td>
				</tr>
				<%
					}
				%>
			</table>

			<input class="calc_price-button" type="submit" name="submit_button"
				value="金額計算" disabled="disabled" /> <input class="register-button"
				type="submit" name="submit_button" value="実行" disabled="disabled" />
		</form>
	</article>
</body>
</html>